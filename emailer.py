import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

def log_email(text):
   print(text)
   log = open("/var/log/voltage/email.log", "a")
   log.write("{}\r\n".format(text))
   log.flush()
   log.close()   

def send(subject, message, logfile=None):
   log_email("Sending email...")
   sender = 'workbench@tjbell.engineering'
   receivers = ['timbell1987@gmail.com', 'dan823232@gmail.com']
   # receivers = ['timbell1987@gmail.com']

   msg = MIMEMultipart()
   msg['From'] = "Workbench <workbench@tjbell.engineering>"
   msg['To'] = "Daniel Israel <dan823232@gmail.com>"
   msg['Date'] = formatdate(localtime=True)
   msg['Subject'] = subject

   msg.attach(MIMEText(message))

   if(logfile):
      with open(logfile, "rb") as f:
         attachment = MIMEApplication(
               f.read(),
               Name=basename(logfile)
         )
      # After the file is closed
      attachment['Content-Disposition'] = 'attachment; filename="%s"' % basename(logfile)
      msg.attach(attachment)

   try:
      with smtplib.SMTP_SSL('mail.tjbell.engineering') as smtpObj:
         print("login")
         smtpObj.login("workbench@tjbell.engineering", "h~gRp,NX.Ovp")
         print("sendmail")
         smtpObj.sendmail(sender, receivers, msg.as_string())         
         log_email("Successfully sent email")
   except Exception as e:
      log_email(e)