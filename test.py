#!/usr/bin/env python3
from spidev import SpiDev
from time import sleep
from datetime import datetime, time, timedelta
import os.path
import atexit
import emailer

dirty = datetime(1, 1, 1, 0, 0)
period = 30 # Time between reports in minutes
logfile = "/var/log/voltage/log_{}.csv".format(datetime.now())

class MCP3008:
    def __init__(self, bus = 0, device = 0):
        self.bus, self.device = bus, device
        self.spi = SpiDev()
        self.open()
        self.spi.max_speed_hz = 1000000 # 1MHz
 
    def open(self):
        self.spi.open(self.bus, self.device)
        self.spi.max_speed_hz = 1000000 # 1MHz
    
    def read(self, channel = 0):
        adc = self.spi.xfer2([1, (8 + channel) << 4, 0])
        data = ((adc[1] & 3) << 8) + adc[2]
        return data
            
    def close(self):
        self.spi.close()

class OutputDevice:
    def __init__(self, pin, direction):
        self.pin = pin
        self.direction = direction
        self.open()
    
    def open(self):
        if not os.path.isdir("/sys/class/gpio/gpio" + str(self.pin)):
            f = open("/sys/class/gpio/export", "a")
            f.write(str(self.pin))
            f.close()
            sleep(0.1)
        self.write("direction", self.direction)
    
    def write(self, addr, val):
        f = open("/sys/class/gpio/gpio" + str(self.pin) + "/" + addr, "a")
        f.write(str(val))
        f.close()
    
    def on(self):
        self.write("value", "1")

    def off(self):
        self.write("value", "0")

pulse = OutputDevice(4, "out")
relay = OutputDevice(17, "out")

def sendPulses(qty):
    for i in range(qty):
        pulse.on()
        sleep(0.01)
        pulse.off()
        sleep(0.01)

def isCheckPoint(now):
    global dirty
    if(now.minute % period == 0 or now.minute == 0):
        now = datetime.now()
        diff = now - dirty
        if(diff > timedelta(minutes=period)):
            dirty = now
            return True
    return False

def isWindow(now):
    if(now > time(12,0,0) and now < time(13,31,0)):
        return True
    if(now > time(15,0,0) and now < time(20,1,0)):
        return True

def cleanup():
    print("\nProgram closing")
    relay.off()

atexit.register(cleanup)

def log_voltage(timestamp, voltage, max, min, avg, message):
    log = open(logfile, "a")
    log.write(','.join([timestamp.strftime("%H:%M:%S"), "{:.2f}".format(voltage), "{:.2f}".format(max), "{:.2f}".format(min), "{:.2f}".format(avg), message]))
    log.write('\r\n')
    log.flush()
    log.close()

def main():
    running = True
    log = open(logfile, "a")
    log.write('"time","voltage","max","min","average","message"\r\n')
    log.flush()
    log.close()
    relay.on()
    adc = MCP3008()
    start = adc.read( channel = 0 ) / 1023.0 * 14.8
    max = start
    min = start
    avg = start
    emailer.send("Started testing", "A power usage test has begun")
    while(running):
        now = datetime.now()
        message = None
        value = adc.read( channel = 0 ) / 1023.0 * 14.8
        print("\r", now.strftime("%H:%M:%S"), "Battery voltage: %.2f" % (value), end='', flush=True)
        if(max < value):
            max = value
        if(min > value):
            min = value
        avg = (avg+value)/2
        if(isCheckPoint(now)):
            message = '"update"'
            if(isWindow(now.time())):
                message = '"vend"'
                sendPulses(15)
            log_voltage(now, start, max, min, avg, message)
            start = value
            max = start
            min = start
            avg = start
        sleep(0.5)
        if(value < 11.6):
            start = value
            message = '"Voltage below minimum, shutting down at {} volts"'.format("{:.2f}".format(value))
            running = False
            log_voltage(now, value, max, min, avg, message)
    print("\n")
    emailer.send("Voltage test process completed", "The power usage test has completed, please see attached csv log file.", logfile)

main()